import discord
from config import DISCORD_TOKEN
from core.constants import  HISTORY_LIMIT, RESULT_LIMIT, PREFIX
from discord.iterators import HistoryIterator
from core.command import Command

class MyClient(discord.Client):
    """
    This is class implements endpoints for messsaging
    """

    async def on_message(self, message):
        """
        This event is called when any messaging happens
        Arguments
            message: This is message object
        """
        if message.author == client.user:
            return

        #Parsing the message and making command of it
        command = Command(message)
        command.run()

        #command.result has two type response itreator or string. Conditions are handled below.
        if type(command.result) is str:
            await message.channel.send(command.result)
        else:
            if isinstance(command.result, HistoryIterator):
                #getting history messages
                history = [Command(elem) async for elem in command.result]
                history = [elem.query for elem in history if elem.action == 'google' and command.query in elem.query]
                history = list(set(history))
                for element in history:
                    await message.channel.send(element)
            else:
                try:
                    counter = RESULT_LIMIT
                    for item in command.result:
                        await message.channel.send(item)
                        counter -= 1
                        if counter < 1:
                            break
                except Exception as e:
                    # if google api exausts the limit
                    result = 'api per limit exceeded'
                    await message.channel.send(result)

client = MyClient()
client.run(DISCORD_TOKEN)
