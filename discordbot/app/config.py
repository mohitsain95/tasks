import os
from dotenv import load_dotenv

#load environment file
env_path = '.env'
load_dotenv(dotenv_path=env_path)

#getting environment variables
DISCORD_TOKEN = os.environ.get('DISCORD_TOKEN', None)
BOT_NAME = os.environ.get('BOT_NAME', None)
