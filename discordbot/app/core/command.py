from googlesearch import search
from core.constants import  PREFIX, HISTORY_LIMIT


class Command:
    """
    This class parse message , extract commands out of it . Methods of this class
    are command itself
    """
    def __init__(self, message):
        """
        This constructor parse message
        Arguments:
            message: message object
        """
        self.result = None
        self.message = message
        self.action = None
        self.query = None
        content = message.content.strip().lower()
        if content.startswith(PREFIX):
            try:
                action, query = content[1:].split(" ", 1)
                self.action  = action
                self.query = query
            except:
                self.action = 'default'
        else:
            self.action = 'default'

    def run(self):
        """
        this method run command on runtime
        """
        method = 'self.{}()'.format(self.action)
        try:
            self.result = eval(method)
        except:
            self.result = 'Command not found. commands supported \n1. !google <keyword>\n2. !recent <keyword>'

    def google(self):
        """
        This is a command method for google search
        """
        result = search(self.query, num=5)
        return result

    def recent(self):
        """
        This is a command method for recent google search
        """
        history = self.message.channel.history(limit=HISTORY_LIMIT)
        return history

    def default(self):
        """
        This is a command method for default search
        """
        if self.message.content.lower() == 'hey':
            return 'hii'
        else:
            return 'I did not understand what you said'
